package database

import (
	"go-rest-api-basic/entities"
	"log"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

//Connector variable used for CRUD operation's
var Connector *gorm.DB

//Connect creates MySQL connection
func Connect(connectionString string) error {
	var err error
	Connector, err = gorm.Open(mysql.Open(connectionString), &gorm.Config{})
	if err != nil {
		log.Fatal(err)
		panic("Cannot connect to DB")
		return err
	}
	log.Println("Connection to DB was successful")
	return nil
}

//Migrate create/updates database table
func Migrate(table *entities.Product) {
	Connector.AutoMigrate(&table)
	log.Println("Table migrated (if not exist before)")
}
