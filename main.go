package main

import (
	"fmt"
	"go-rest-api-basic/controllers"
	"go-rest-api-basic/database"
	"go-rest-api-basic/entities"
	"go-rest-api-basic/util"

	"log"
	"net/http"

	"github.com/gorilla/mux"
	_ "gorm.io/driver/mysql"
)

func main() {
	// Load Configurations from config.json using Viper
	util.LoadAppConfig()

	// Initialize Database
	initDB()

	// Initialize the router
	myRouter := mux.NewRouter().StrictSlash(true)

	// Register Routes
	initHandleRequests(myRouter)

	// Start the server
	log.Println(fmt.Sprintf("Starting the server on port %s", util.AppConfig.Port))
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%v", util.AppConfig.Port), myRouter))
}

func initDB() {
	err := database.Connect(util.AppConfig.ConnectionString)
	if err != nil {
		panic(err.Error())
	}
	database.Migrate(&entities.Product{})
}

func initHandleRequests(myRouter *mux.Router) {
	myRouter.HandleFunc("/", homePage)
	myRouter.HandleFunc("/api/products", controllers.CreateProduct).Methods("POST")
	myRouter.HandleFunc("/api/products", controllers.GetProducts).Methods("GET")
	myRouter.HandleFunc("/api/products/{id}", controllers.GetProduct).Methods("GET")
	myRouter.HandleFunc("/api/products/{id}", controllers.UpdateProduct).Methods("PUT")
	myRouter.HandleFunc("/api/products/{id}", controllers.DeleteProduct).Methods("DELETE")
}

func homePage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "Welcome!")
}
