# Go REST API Basic

Go REST API Basic - Simple REST API Application

Simple app to demonstrate Golang and MySQL as backend server.

Some dependency : Gorilla Mux (routes and HTTP handlers), Gorm (ORM for MySQL), MySQL and Viper (configuration manager)

## Installation

Clone the project

```bash
git clone xxx
```

## Usage

```go

cd folder_name

# execute the file
go run main.go

```

## Reference

1st: https://codewithmukesh.com/blog/implementing-crud-in-golang-rest-api/

2nd: https://levelup.gitconnected.com/build-a-rest-api-using-go-mysql-gorm-and-mux-a02e9a2865ee

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)