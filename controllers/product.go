package controllers

import (
	"encoding/json"
	"go-rest-api-basic/database"
	"go-rest-api-basic/entities"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func CreateProduct(w http.ResponseWriter, r *http.Request) {
	// fmt.Fprint(w, "Create Product")
	payloads, _ := ioutil.ReadAll(r.Body)

	var product entities.Product

	// casting
	json.Unmarshal(payloads, &product)
	// insert using gorm
	database.Connector.Create(&product)

	// create response
	res := entities.Result{Code: 200, Data: product, Message: "Success create product"}
	result, err := json.Marshal(res)

	if err != nil {
		log.Println("Process failed", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	// create response
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(result)
}

func GetProducts(w http.ResponseWriter, r *http.Request) {
	products := []entities.Product{}

	database.Connector.Find(&products)

	// create response
	res := entities.Result{Code: 200, Data: products, Message: "Success get products"}
	results, err := json.Marshal(res)

	if err != nil {
		log.Println("Process failed", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	// create response
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(results)
}

func GetProduct(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	productID := vars["id"]

	var product entities.Product
	// dbResult := database.Connector.First(&product, productID)
	dbResult := database.Connector.Where("id = ?", productID).Limit(1).Find(&product)

	// create response
	// res := entities.Result{Code: 200, Data: product, Message: "Success get product"}

	// init var
	res := entities.Result{}

	// initiate response
	w.Header().Set("Content-Type", "application/json")

	if dbResult.RowsAffected == 0 || dbResult.Error != nil {
		// create response content
		res = entities.Result{Code: 404, Message: "Fail get product"}
		w.WriteHeader(http.StatusNotFound)
	} else {
		// create response content
		res = entities.Result{Code: 200, Data: product, Message: "Success get product"}
		w.WriteHeader(http.StatusOK)
	}

	result, err := json.Marshal(res)

	if err != nil {
		log.Println("Process failed", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	w.Write(result)
}

func UpdateProduct(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	productID := vars["id"]

	// fmt.Fprint(w, "Create Product")
	payloads, _ := ioutil.ReadAll(r.Body)

	var productUpdates entities.Product

	// casting
	json.Unmarshal(payloads, &productUpdates)

	var product entities.Product
	// dbResult := database.Connector.First(&product, productID)

	// change query since record not found print into console
	dbResult := database.Connector.Where("id = ?", productID).Limit(1).Find(&product)

	// log.Println("Result RowsAffected:", dbResult.RowsAffected)
	// log.Println("Result Error:", dbResult.Error)

	// initiate var
	res := entities.Result{}

	w.Header().Set("Content-Type", "application/json")

	// create response
	// res := entities.Result{Code: 200, Data: product, Message: "Success update product"}

	if dbResult.RowsAffected == 0 || dbResult.Error != nil {
		// create response
		res = entities.Result{Code: 404, Message: "Fail update product"}
		w.WriteHeader(http.StatusNotFound)
	} else {
		database.Connector.Model(&product).Updates(productUpdates)
		// create response
		res = entities.Result{Code: 200, Data: product, Message: "Success update product"}
		w.WriteHeader(http.StatusOK)
	}

	result, err := json.Marshal(res)

	if err != nil {
		log.Println("Process failed", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	w.Write(result)
}

func DeleteProduct(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	productID := vars["id"]

	var product entities.Product
	// checkData := database.Connector.First(&product, productID)
	// dbResult := database.Connector.Where("id = ?", productID).First(&product)
	dbResult := database.Connector.Where("id = ?", productID).Limit(1).Find(&product)

	// initiate var
	res := entities.Result{}

	w.Header().Set("Content-Type", "application/json")

	if dbResult.RowsAffected == 0 || dbResult.Error != nil {
		// create response
		res = entities.Result{Code: 404, Message: "Fail delete product"}
		w.WriteHeader(http.StatusNotFound)
	} else {
		database.Connector.Delete(&product)
		// create response
		res = entities.Result{Code: 200, Message: "Success delete product"}
		w.WriteHeader(http.StatusOK)
	}

	result, err := json.Marshal(res)

	if err != nil {
		log.Println("Process failed", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	// log.Println("Responding")

	w.Write(result)

}
